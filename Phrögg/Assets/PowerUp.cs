﻿//Written by: August Möller

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    private void Update()
    {
        transform.Translate(Vector2.up * -4 * Time.deltaTime, Space.World); //power up falls down
    }
    private void OnTriggerEnter2D(Collider2D collision) //when touching player dissapear
    {
        if (collision.tag == "Player")
        {
            Destroy(gameObject);
        }
    }
}
