﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CoinsEarnedInRun : MonoBehaviour
{
    [SerializeField]private TMP_Text coinsEarnedText;
    private int coinsEarned;
    private PlayerCoins playercoins;
    private PlayerEntity playerEntity;
    
    public static CoinsEarnedInRun instance;


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }
    
    private void Start()
    {
        PlayerCoins.instance.sessionCoins = 0;
        coinsEarnedText = GetComponent<TMP_Text>();
        playerEntity = FindObjectOfType<PlayerEntity>();
    }
    
    public void EarnedCoins()
    {
        coinsEarned = PlayerCoins.instance.sessionCoins;
        coinsEarnedText.text = coinsEarned.ToString();
    }
}
