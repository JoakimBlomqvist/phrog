﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameServices;
using UnityEngine;
using UnityEngine.UI;

public class Portrait : MonoBehaviour
{
    private void Start()
    {
        GetComponent<Image>().sprite = GameManager.instance.GetCharacter().Portrait;
    }
}
