﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    public void ReloadScene()
    {
        Time.timeScale = 1;
        PlayerCoins.instance.sessionCoins = 0;
        SceneManager.LoadScene("August Scene");
    }
    public void LoadMenu()
    {
        Time.timeScale = 1;
        PlayerCoins.instance.sessionCoins = 0;
        SceneManager.LoadScene("MainMenu");
    }

    public void LoadShop()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Shop");
    }

    public void Exit()
    {
        Application.Quit();
    }
}
