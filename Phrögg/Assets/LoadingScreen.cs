﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameServices;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingScreen : MonoBehaviour
{
    private void Awake()
    {
        StartCoroutine(Wait());
    }


    public IEnumerator Wait(){
    
    yield return new WaitForSeconds(1);
    SceneManager.LoadScene("MainMenu");

    } 

}
