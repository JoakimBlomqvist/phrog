﻿using System.Collections;
using System.Collections.Generic;
using System.Data.SqlTypes;
using UnityEngine;

public class MenuScript : MonoBehaviour
{
    public GameObject gameManager;
    public static bool GameIsPaused;

    public GameObject PauseMenuUI;

    public GameObject ConfirmMenuUI;

    public GameObject EndMenuUI;

    public GameLoop gameLoop;
    // Start is called before the first frame update
    void Start()
    {
        GameIsPaused = false;
        gameLoop = gameManager.GetComponent<GameLoop>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && EndMenuUI.activeSelf == false)
        {
            if (PauseMenuUI.activeSelf == true)
            {
                ResumeGame();
            }
            else if (ConfirmMenuUI.activeSelf == true)
            {
                BackToPause();
            }
            else
            {
                PauseGame();
            }
        }
        EndMenu();
    }

    public void ResumeGame()
    {
        PauseMenuUI.SetActive(false);
        ConfirmMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
    }
    
    public void PauseGame()
    {
        PauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    public void MainMenu()
    {
        ConfirmMenuUI.SetActive(true);
        PauseMenuUI.SetActive(false);
    }

    public void BackToPause()
    {
        PauseMenuUI.SetActive(true);
        ConfirmMenuUI.SetActive(false);
    }

    public void EndMenu()
    {
        if (gameLoop.playerAlive == false)
        {
            EndMenuUI.SetActive(true);
            Time.timeScale = 0f;
        }
    }


}
