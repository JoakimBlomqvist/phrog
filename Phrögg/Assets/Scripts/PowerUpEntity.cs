﻿//Written by: August Möller
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpEntity : Entity
{
    private float speed = 5;
    public int id;
    private GameObject playerTarget;
    private CircleCollider2D circleCollider2D;
    public SpriteRenderer spriteRenderer;
    void Start()
    {
        circleCollider2D = GetComponent<CircleCollider2D>();
        if (id >= 1) //set it so the power up ship follows the players left or right depending on what id it has
        {
            playerTarget = GameObject.Find("SmallShipTarget1");
        } else
        {
            playerTarget = GameObject.Find("SmallShipTarget2");
        }
        
    }

    private void OnTriggerEnter2D(Collider2D collision) //when touching the player projectile take damage
    {
        if (collision.tag == "EnemyProjectile" || collision.tag == "Enemy")
        {
            TakeDamage();
        }
    }

    void Update()
    {
        if(playerTarget != null)// if there is a player in the scene
        {
            if (playerTarget.active == false)
            {
                dead = true;
                circleCollider2D.enabled = false;
                spriteRenderer.color = new Color(.5f, .5f, .5f);
            }
            else
            {
                dead = false;
                circleCollider2D.enabled = true;
                spriteRenderer.color = new Color(1, 1, 1);
            }
            float step = speed * Time.deltaTime;
            transform.position = Vector2.MoveTowards(transform.position, playerTarget.transform.position, step);
        } else
        {
            Debug.Log("please put player in the scene");
        }
    }
}
