﻿//Written by: August Möller

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    //[HideInInspector]
    public float speed;
    public float direction;
    public int damage;

    void Update()
    {
        transform.Translate( DegreeToVector2(direction) * (speed * Time.deltaTime), Space.World); //move
        if(transform.position.y >= 6 || transform.position.y <= -6) //if projectile is too far up or too far down, despawn the projectile
        {
            Destroy(gameObject);
        }
    }
    
    private static Vector2 RadianToVector2(float radian)
    {
        return new Vector2(Mathf.Cos(radian), Mathf.Sin(radian));
    }
    private static Vector2 DegreeToVector2(float degree)
    {
        return RadianToVector2(degree * Mathf.Deg2Rad);
    }
    
    
}
