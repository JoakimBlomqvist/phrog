﻿//Written by: August Möller

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchControls : MonoBehaviour
{
    private float speed = 20;

    void Update()
    {
        float step = speed * Time.deltaTime; //this makes it so the movement gets more smooth

        if (Input.touchCount > 0) //if there is more than zero things touching the screen
        {
            Touch touch = Input.GetTouch(0); //assign the variable for first thing that touches the screen
            Vector2 touchPosition = Camera.main.ScreenToWorldPoint(touch.position); //assign the cordinates for the touch and it will make it so the screen size doesn't matter
            transform.position = Vector2.MoveTowards(transform.position, touchPosition, step);
        }
    }
}
