﻿//Written by: August Möller

using System;
using System.Collections;
using System.Collections.Generic;
using GameServices;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    private Entity entity;
    private Weapon weapon;
    public int powerUpLvl = 0;
    public WeaponCollection weaponCollection;
    
    [SerializeField]
    private Transform scaler;
    private Vector3 scale;
    
    private void Awake()
    {
        //weapon = GetComponent<Weapon>();
        entity = GetComponent<Entity>();
        Init(weaponCollection);
    }


    public void Init(WeaponCollection wep)
    {
        weaponCollection = wep;
        weapon = weaponCollection.Weapons[0];
        
        //InvokeRepeating("Shoot", fireRate, fireRate);
        if (scaler != null)
        {
            scale = scaler.localScale;
        }
        StartCoroutine(RepeatShooting()); //coroutine that repeats and how fast depends on the fire rate set
        
        
    }



    IEnumerator RepeatShooting()
    {
        yield return new WaitForSeconds(weapon.fireRate);
        while (true)
        {
            if (scaler != null)
            {
                scaler.localScale = new Vector3(scale.x*1.2f,scale.y*.7f,scale.z);
                scaler.LeanScale(scale, .2f);
            }
            Shoot(); //the speed for these are set 
            yield return new WaitForSeconds(weapon.fireRate);
        }
    }

    void Shoot()
    {
        if (!entity.dead) //if the entity isnt dead
        {
            weapon.FireWeapon(transform.position);
        }
    }
    
    public void PowerUpLevel()
    {
        SetPowerUpLevel(powerUpLvl + 1);
    }
    public void SetPowerUpLevel(int lvl)
    {
        powerUpLvl = lvl;
        if (powerUpLvl > weaponCollection.Weapons.Count -1)
        {
            powerUpLvl = weaponCollection.Weapons.Count -1;
        }
        weapon = weaponCollection.Weapons[powerUpLvl];
    }
}
