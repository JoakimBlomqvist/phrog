﻿//Written by: Joakim Blomqvist

using System.Collections;
using System.Collections.Generic;
using GameServices;
using UnityEngine;

//Script for how enemies spawns in waves.
public class EnemySpawner : MonoBehaviour
{
    public int waveCount = 0;
    
    [SerializeField] private List<WaveConfig> waveConfigs;
    [SerializeField] private bool looping = false;
    [SerializeField] private int startingWave = 0;
    [SerializeField] private int waveIndex = 0;
    [SerializeField] private int totalWaves = 20;
    [SerializeField] private AnalyticsManager analytics;

    private BossEntity bossUnit;
    private WaveNotifier waveText;
    
    IEnumerator Start()
    {
        analytics = AnalyticsManager.instance;
        waveText = FindObjectOfType<WaveNotifier>();
        
        var currentWave = waveConfigs[startingWave];
        
        do
        {
            yield return StartCoroutine(SpawnAllWaves());
        } while (looping);
        //If looping is checked it goes on endlessly.
    }

    //Starts an Coroutine for spawning waves.
    private IEnumerator SpawnAllWaves()
    {
        //Spawns a random wave within the waveconfig index and counts what wave it currently is.
        for (waveCount = startingWave; waveCount <= totalWaves; waveCount++)
        {
            waveIndex = Random.Range(0, waveConfigs.Count - 1);
            var currentWave = waveConfigs[waveIndex];
            
            waveText.MessageWave();
            
            //Every 10th wave spawn a boss wave
            if (waveCount % 10 == 0 && waveCount > 0)
            {
                //Tells analytics that the boss level has been reached
                if(analytics != null){analytics.LevelCapReached("123", "Boss level");}
                //Spawns the wave chosen. 
                yield return StartCoroutine(SpawnBossWave(waveConfigs[waveConfigs.Count-1]));
            }
            else
            {
                yield return StartCoroutine(SpawnAllEnemiesInWave(currentWave));
            }
        }
    }
    
    //Spawns all enemies in the wave according to the NumberOfEnemies set in the inspector
    private IEnumerator SpawnAllEnemiesInWave(WaveConfig waveConfig)
    {
        for (int enemyCount = 0; enemyCount < waveConfig.GetNumberOfEnemies(); enemyCount++)
        {
            var newEnemy = Instantiate(waveConfig.GetEnemyPrefab());
               //Use if waypoints: *waveConfig.GetWaypoints()[0].transform.position, Quaternion.identity);*
            
            //Tells the enemy script which waveconfig its related to
            newEnemy.GetComponent<EnemyLeanTween>().SetWaveConfig(waveConfig);
            
            if (waveConfig.GetNumberOfEnemies() == enemyCount + 1)
            {
                //Time until the next wave spawns
                yield return new WaitForSeconds(waveConfig.GetTimeBetweenWaves());
            }
            else
            {
                //The delay between each enemy spawn in the current wave
                yield return new WaitForSeconds(waveConfig.GetTimeBetweenSpawns());
            }
        }
    }

    private IEnumerator SpawnBossWave(WaveConfig waveConfig)
    {
        //Spawn bosswave
        Debug.Log("Boss wave");
        
        var newEnemy = Instantiate(waveConfig.GetEnemyPrefab());
            //Use if waypoints: *waveConfig.GetWaypoints()[0].transform.position, Quaternion.identity);*

        //Tells the enemy script which waveconfig its related to
        newEnemy.GetComponent<EnemyLeanTween>().SetWaveConfig(waveConfig);
        bossUnit = newEnemy.GetComponent<BossEntity>(); 
        
        //Return when the boss dies.
        yield return new WaitUntil(() => bossUnit.dead);
    }
}
