﻿//Written by: Joakim Blomqvist

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Script for how enemies move
public class EnemyLeanTween : MonoBehaviour
{
    [SerializeField] private GameObject enemy;
    [SerializeField] private WaveConfig waveConfig;
    [SerializeField] private GameObject spawn;
    public enum MovePattern
    {
        Movement1,
        Movement2,
        Movement3
    }

    public MovePattern movePattern;

    [SerializeField]private float Movespeed;
  
    //Gets enemy spawnpoint and gets gameobject this script is attached to.
    void Awake()
    {
        enemy = gameObject;
        spawn = GameObject.FindGameObjectWithTag("EnemySpawn");
    }

    //Gets the movement speed from the waveconfig related to the enemy.
    private void Start()
    {
        Movespeed = waveConfig.GetMoveSpeed();
        transform.position = spawn.transform.position;
        Tween();
    }

    //Function to get what waveconfig is related to this enemy.
    public void SetWaveConfig(WaveConfig waveConfig)
    {
        this.waveConfig = waveConfig;
    }

    //How the enemy will move.
    private void Tween()
    {
        switch (movePattern)
        {
            case MovePattern.Movement1:
                LeanTween.moveX(enemy, 3f, Movespeed).setEaseInCubic().setLoopPingPong();
                LeanTween.moveY(enemy, 3, 5).setEaseInCubic().setLoopPingPong();
                break;
            
            case MovePattern.Movement2:
                LeanTween.moveX(enemy, 2.5f, Movespeed).setEaseInBounce().setLoopPingPong();
                LeanTween.moveY(enemy, 3, 5).setEaseOutElastic().setLoopPingPong();
                break;
            
            case MovePattern.Movement3:
                LeanTween.moveX(enemy, 2f, Movespeed).setEaseInOutBounce().setLoopPingPong();
                LeanTween.moveY(enemy, 3, 6).setEaseInOutCirc().setLoopPingPong();
                break;
        }
    }
}
