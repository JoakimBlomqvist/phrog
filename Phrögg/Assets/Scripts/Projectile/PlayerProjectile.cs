﻿//Written by: August Möller

using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class PlayerProjectile : Projectile
{
    public GameObject explosion;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            Instantiate(explosion,transform.position,quaternion.identity);
            Destroy(gameObject);
        }
    }
}
