﻿//Written by: August Möller
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
    [HideInInspector]
    public GameObject pauseUI;
    [HideInInspector]
    public GameObject pauseButton;
    public bool isPaused;
    // Start is called before the first frame update
    void Start()
    {
        pauseUI = GameObject.Find("PauseUI");
        pauseButton = GameObject.Find("PauseButton");
        if (pauseUI != null && pauseButton != null) //if there is no pause button or pause ui in scene console log will tell you to add it
        {
            pauseUI.GetComponentInChildren<Canvas>().enabled = false;
            pauseButton.GetComponentInChildren<Canvas>().enabled = true;
        }
        else
        {
            Debug.Log("need a pause ui and or pause button");
        }

    }

    public void PauseGame()
    {
        if(pauseUI != null && pauseButton != null) //if there is no pause button or pause ui in scene console log will tell you to add it
        {
            //if the game wasn't paused when you pressed the pause button the isPaused is set to true and the time freezes and the pause ui appears
            if (!isPaused)
            {
                Time.timeScale = 0; //when time scale is 0 the game freezes
                isPaused = true;
                pauseUI.GetComponentInChildren<Canvas>().enabled = true; //make the pause menu appear
                pauseButton.GetComponentInChildren<Canvas>().enabled = false;
            }
            else
            {
                //if it was paused the opposite happens
                Time.timeScale = 1;
                isPaused = false;
                pauseUI.GetComponentInChildren<Canvas>().enabled = false; //make the pause menu dissapear
                pauseButton.GetComponentInChildren<Canvas>().enabled = true;
            }
        }
        else
        {
            Debug.Log("need a pause ui and or pause button");
        }
    }
}
