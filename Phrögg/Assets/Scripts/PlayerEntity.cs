﻿//Written by: August Möller

using System.Collections;
using System.Collections.Generic;
using GameServices;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.UI;

public class PlayerEntity : Entity
{
    public int lives;
    public int damage;
    public Text livesCounter;
    public int powerUpLvl = 0;
    public bool invincible;

    public SpriteRenderer spriteRenderer;
    private TouchControls touchControls;
    private CircleCollider2D circleCollider2D;
    private Shooting Shooting;

    //Analytics
    private Dictionary<string, object> customEventParams = new Dictionary<string, object>();

    [HideInInspector]
    public GameObject gameOverUI;

    public GameObject smallShipTarget1;
    public GameObject smallShipTarget2;

    private void Start()
    {
        touchControls = GetComponent<TouchControls>();
        circleCollider2D = GetComponent<CircleCollider2D>();
        Shooting = GetComponent<Shooting>();

        Character character = GameManager.instance.GetCharacter();
        
        Shooting.Init(character.WeaponCollection);
        Animator anim = GetComponent<Animator>();
        anim.SetTrigger(character.Name);
        
        
        gameOverUI = GameObject.Find("GameOverUI");

        if (gameOverUI != null) //if there is no game over ui the console will tell you to ad one
        {
            gameOverUI.GetComponentInChildren<Canvas>().enabled = false;
        }
        else
        {
            Debug.Log("Please place a game over ui in this scene");
        }
        /*smallShipTarget1 = GameObject.Find("SmallShipTarget1");
        smallShipTarget2 = GameObject.Find("SmallShipTarget2");*/
        
    }



    private void Update()
    {
        if (livesCounter != null) //this will make it so if the temporary lives counter isn't on the scene there wont be an error
        {
            livesCounter.text = lives.ToString();
        }

        if (dead)
        {
            smallShipTarget1.active = false;
            smallShipTarget2.active = false;
        }
        else
        {
            smallShipTarget1.active = true;
            smallShipTarget2.active = true;
        }

    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!invincible) //when you aren't invincible you will take damage (die) when touching enemy projectile or the enemy
        {
            if (collision.tag == "EnemyProjectile" || collision.tag == "Enemy")
            {
                TakeDamage();
            }
        }

        if (collision.tag == "PowerUp")
        {
            Shooting.PowerUpLevel();
        }
    }
    /*public override void TakeDamage()
    {
        hp -= 1;
    }*/
    public override void Die()
    {
        /*dead = true;
        gameObject.active = false;*/
        StartCoroutine(DeathSequence());
    }

    private IEnumerator DeathSequence()
    {
        yield return StartCoroutine(Dead()); //will remove the movement and collider and sprite making it look like it dissapear and remove a life and take you back to level 0 and make you stop shooting
        if(lives >= 1) //if you have more than 1 life when you die yoi will be revived
        {
            yield return StartCoroutine(Revived()); //will revert what happens when you die and respawn you to where you were
            yield return StartCoroutine(Invincibility()); //will make it so you wont die afer respawning so you wont get killed instantly when respawning
            yield return StartCoroutine(InvincibilityStop()); //will remove invincibility
        } else //if you only have one life you get game over
        {
            GameOver();
        }
        
    }
    IEnumerator Dead()
    {
        
        dead = true;
        spriteRenderer.enabled = false;
        touchControls.enabled = false;
        circleCollider2D.enabled = false;
        lives--; //lose a life
        Shooting.SetPowerUpLevel(0);
        yield return new WaitForSeconds(1);
    }

    IEnumerator Revived()
    {
        dead = false;
        hp = 1;
        //gameObject.active = true;
        spriteRenderer.enabled = true;
        touchControls.enabled = true;
        circleCollider2D.enabled = true;
        transform.position = new Vector2(0, -4);
        yield return null;
    }

    IEnumerator Invincibility()
    {
        invincible = true; //you are invicible
        spriteRenderer.color = new Color(.5f,.5f, .5f);
        yield return new WaitForSeconds(2);
    }
    IEnumerator InvincibilityStop()
    {
        invincible = false;
        spriteRenderer.color = new Color(1, 1, 1);
        yield return null;
    }

    void GameOver()
    {
        //Analytics
        AnalyticsResult eventResultPlayerDied = AnalyticsEvent.Custom("Player_GameOver", customEventParams);
        
        CoinsEarnedInRun.instance.EarnedCoins();
        PlayerCoins.instance.playerCoins += PlayerCoins.instance.sessionCoins;
        PlayerCoins.instance.updatePlayerPrefs();
        
        if (gameOverUI != null)
        {
            gameOverUI.GetComponentInChildren<Canvas>().enabled = true; //the game over screen appears
        }
        Time.timeScale = 0;
    }
}