﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "WeaponCollection")]
public class WeaponCollection : ScriptableObject
{
    public List<Weapon> Weapons;
}
