﻿//Written by: August Möller

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{
    public int hp = 1;
    public bool dead;

    public virtual void TakeDamage(int damage = 1)
    {
        hp -= damage;
        if (hp < 1 && !dead)
        {
            Die();
        }
        
    }

    public virtual void Die()
    {
        //Debug.Log("Enemy dead");
        dead = true;
        Destroy(gameObject);
        //gameObject.active = false;
    }
}
