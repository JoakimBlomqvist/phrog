﻿//Written by: Joakim Blomqvist

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Notifies what wave the player is currently on
public class WaveNotifier : MonoBehaviour
{
    [SerializeField] private Text waveText;
    [SerializeField] private EnemySpawner _enemySpawner;
    
    void Awake()
    {
        waveText = GetComponent<Text>();
        _enemySpawner = FindObjectOfType<EnemySpawner>();
    }

    public void MessageWave()
    {
        //Every 10th wave announces that its a boss fight
        if (_enemySpawner.waveCount % 10 == 0 && _enemySpawner.waveCount > 0)
        {
            waveText.text = "UH OH! BOSS FIGHT";
        }
        else
        {
            waveText.text =  "Wave: " + _enemySpawner.waveCount;
        }
    }
}
