﻿//Written by: Joakim Blomqvist

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Enemy Wave Configuration")]
public class WaveConfig : ScriptableObject
{
   [SerializeField] private GameObject enemyPrefab;
   [SerializeField] private GameObject pathPrefab;
   [SerializeField] private float timeBetweenSpawns = 0.5f;
   [SerializeField] private float spawnRandomRate = 0.3f;
   [SerializeField] private int numberOfEnemies = 5;
   [SerializeField] private float moveSpeed = 2f;
   [SerializeField] private float timeBetweenWaves = 5f;

   public GameObject GetEnemyPrefab()
   {
      return enemyPrefab;
   }
   
   //When using waypoints. Not needed in leantween pathing.
   public List<Transform> GetWaypoints()
   {
      var waveWaypoints = new List<Transform>();
      foreach (Transform child in pathPrefab.transform)
      {
         waveWaypoints.Add(child);
      }
      
      return waveWaypoints;
   }
   
   public float GetTimeBetweenSpawns()
   {
      return timeBetweenSpawns;
   }
   
   public float GetTimeBetweenWaves()
   {
      return timeBetweenWaves;
   }
   
   public float GetSpawnRandomRate()
   {
      spawnRandomRate = Random.Range(0.2f, 1f);
      
      return spawnRandomRate;
   }
   
   public int GetNumberOfEnemies()
   {
      return numberOfEnemies;
   }

   public float GetMoveSpeed()
   {
      return moveSpeed;
   }

}
