﻿//Written By Joakim Blomqvist

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropCoinsOnDeath : MonoBehaviour
{
    [SerializeField]private PlayerCoins playerC;

    [SerializeField] private int coinsAmount = 2;

    private void Start()
    {
        playerC = PlayerCoins.instance;
    }

    //OnDestroy drop coins equal to coinsAmount and add it to sessionCoins
    private void OnDestroy()
    {
        playerC.sessionCoins += coinsAmount;
    }
}
