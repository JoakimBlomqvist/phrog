﻿//Written by: Joakim Blomqvist

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

namespace GameServices
{
   public class AnalyticsManager : MonoBehaviour, IService
   {
      private Dictionary<string, object> customEventParams = new Dictionary<string, object>();
      public static AnalyticsManager instance;


      private void Awake()
      {
         if (instance == null)
         {
            instance = this;
         }
         else
         {
            Destroy(this);
         }
      }
      
      public void Initialize()
      {
         AnalyticsEvent.GameStart();
         Debug.Log("Analytics on");
      }

      public void LevelCapReached(string playerID, string levenName)
      {
         customEventParams.Clear();
         customEventParams.Add("player_id", playerID);
         customEventParams.Add("leven_Name", levenName);

         AnalyticsResult eventResult = AnalyticsEvent.Custom("level_cap_reached", customEventParams);
         AnalyticsResult levelComplete = AnalyticsEvent.LevelStart(levenName, customEventParams);
         
         Debug.LogFormat("Custom event fired => {0}", eventResult);
      }
   }
}
