﻿//Written by: Joakim Blomqvist

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.Analytics;
using UnityEngine.Monetization;
using ShowResult = UnityEngine.Advertisements.ShowResult;

namespace GameServices
{
    public class AdsManager : MonoBehaviour, IUnityAdsListener, IService
    {
        //dont change this....
        private string projectPlayStoreId = "4015343"; //Android ID
        [SerializeField] private bool testMode = true;
        [SerializeField] private PlayerCoins playerCoins;

        private Dictionary<string, object> customEventParams = new Dictionary<string, object>();

        private void Start()
        {
            playerCoins = GetComponent<PlayerCoins>();
        }
        public void Initialize()
        {
#if UNITY_IOS || UNITY_ANDROID
            Debug.Log("Ads initialized");
            Advertisement.AddListener(this);
            Advertisement.Initialize(projectPlayStoreId, testMode);
#else
            Debug.LogWarning("The current platform doesn't support Unity Monetization");
            
#endif
        }

        //this is the method you call from buttons to Show an ad
        public void ShowAd(string p) 
        {
            Advertisement.Show(p);
        }


        public void OnUnityAdsDidFinish(string placementId, ShowResult showResult) 
        {
            if(showResult == ShowResult.Finished) 
            {
                switch (placementId) 
                {
                    case "rewardedVideo":
                        //Call the scripts method that you want to add stuff to
                        Debug.Log("Rewarded");
                        AnalyticsResult eventResult = AnalyticsEvent.AdStart(true, null, "rewardedVideo", customEventParams);
                        Debug.LogFormat("Event result {0}", eventResult);
                        
                        break;
                    case "extraCoins":
                        //Double the players current coins earned in the session and adds it to the "player wallet"
                        playerCoins.DoubleCoins();
                        AnalyticsResult eventResultExtraCoins = AnalyticsEvent.AdStart(true, null, "extraCoins", customEventParams);
                        Debug.LogFormat("Event result {0}", eventResultExtraCoins);
                        break;
                }

            }
            else if(showResult == ShowResult.Failed) {
                Debug.Log("Failed");
            }
        }

        public void OnUnityAdsReady(string placementId)
        {

        }

        public void OnUnityAdsDidError(string message)
        {

        }

        public void OnUnityAdsDidStart(string placementId)
        {

        }
    }
}

