﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameServices
{
    
    public class GameManager : MonoBehaviour
    {
        private int coins;
        
        private int leftSmallIndex;
        private int RightSmallIndex;

        private Character currentCharacter;
        
        public static GameManager instance;

        public List<Character> characters;
        
        public Dictionary<string, int> UnlockedCharacters = new Dictionary<string, int>();



        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
                DontDestroyOnLoad(this);
                foreach (var character in characters)
                {
                    string key = character.Name + "Unlocked";
                    
                    if (PlayerPrefs.HasKey(key))
                    {
                        UnlockedCharacters.Add(character.Name,PlayerPrefs.GetInt(key));
                    }
                    else
                    {
                        int unlocked = 0;
                        if (character.Name == "Phrögger")
                        {
                            unlocked++;
                            PlayerPrefs.SetString("CurrentCharacter",character.Name);
                        }
                        UnlockedCharacters.Add(character.Name,unlocked);
                        PlayerPrefs.SetInt(key,unlocked);
                    }
                }

                currentCharacter = characters.Find( c => c.name == PlayerPrefs.GetString("CurrentCharacter"));
                if (currentCharacter == null)
                {
                    currentCharacter = characters[0];
                }

                PlayerPrefs.Save();
                
                return;
            }
            Destroy(this);
        }

        public Character GetCharacter()
        {
            return currentCharacter;

        }

        public void SetCharacter(Character character)
        {
            currentCharacter = character;
            PlayerPrefs.SetString("CurrentCharacter",character.Name);
            PlayerPrefs.Save();
        }

        public void UnlockCharacter(Character character)
        {
            string key = character.Name + "Unlocked";
            UnlockedCharacters.Remove(character.Name);
            UnlockedCharacters.Add(character.Name,1);
            PlayerPrefs.SetInt(key,1);
            PlayerPrefs.Save();
        }


        public bool IsCharacterUnlocked(Character character)
        {
            int unlocked = 0;
            UnlockedCharacters.TryGetValue(character.Name, out unlocked);
            
            return (unlocked>0);
        }




    }


}