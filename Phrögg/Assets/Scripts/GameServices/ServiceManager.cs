﻿//Written by: Joakim Blomqvist

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameServices
{
    public class ServiceManager : MonoBehaviour
    {
        public static ServiceManager instance = null;
        public static AdsManager Ads { get; private set; }
        public static AnalyticsManager Analytics { get; private set; }
        private List<IService> _serviceManagers = new List<IService>();
        // Start is called before the first frame update
        void Start()
        {
            InitializeServices();
            
            if (instance == null) instance = this;
            else if (instance != null) Destroy(gameObject);
            
            DontDestroyOnLoad(gameObject);
        }

        // Update is called once per frame
        private void InitializeServices()
        {
            Ads = GetComponent<AdsManager>();
            Analytics = GetComponent<AnalyticsManager>();
            _serviceManagers.Add(Analytics);
            _serviceManagers.Add(Ads);
            
            
            foreach (IService service in _serviceManagers)
            {
                service.Initialize();
            }
        }
    }
}

