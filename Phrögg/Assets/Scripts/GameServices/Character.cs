﻿using UnityEngine;

namespace GameServices
{
    [CreateAssetMenu(fileName = "Character", menuName = "Character")]
    public class Character : ScriptableObject
    {
        public string Name;
        public string Description;
        public int Cost;
        public Sprite Portrait;
        public WeaponCollection WeaponCollection;
    }
}