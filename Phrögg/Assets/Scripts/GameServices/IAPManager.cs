﻿//Written by Joakim Blomqvist

using System;
using GameServices;
using TMPro;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;

namespace GameServices
{
    public class IAPManager : MonoBehaviour, IStoreListener, IService
    {
        public static IAPManager instance;
        private PlayerCoins playerCoins;

        private static IStoreController m_StoreController;
        private static IExtensionProvider m_StoreExtensionProvider;

        //Create your products
        private string coins1000 = "coins_1000";
        private string ship = "ship";


        public void InitializePurchasing()
        {
            if (IsInitialized())
            {
                return;
            }

            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

            //Step 2 choose if your product is a consumable or non consumable
            builder.AddProduct(coins1000, ProductType.Consumable);
            builder.AddProduct(ship, ProductType.NonConsumable);

            UnityPurchasing.Initialize(this, builder);
        }


        private bool IsInitialized()
        {
            return m_StoreController != null && m_StoreExtensionProvider != null;
        }


        //Create methods
        public void BuyCoins1000()
        {
            BuyProductID(coins1000);
        }

        public void BuyShip()
        {
            BuyProductID(ship);
        }



        //Modify purchasing
        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
        {
            if (String.Equals(args.purchasedProduct.definition.id, coins1000, StringComparison.Ordinal))
            {
                playerCoins.playerCoins += 1000;

                Debug.Log("Added 1000 coins");
            }
            else if (String.Equals(args.purchasedProduct.definition.id, ship, StringComparison.Ordinal))
            {
                Debug.Log("Bought a ship");
            }
            else
            {
                Debug.Log("Purchase Failed");
            }

            return PurchaseProcessingResult.Complete;
        }




        /*************************************************/
        private void Awake()
        {
            TestSingleton();
        }

        public void Initialize()
        {
            if (m_StoreController == null)
            {
                InitializePurchasing();
            }

            playerCoins = GetComponent<PlayerCoins>();
        }

        private void TestSingleton()
        {
            if (instance != null)
            {
                Destroy(gameObject);
                return;
            }

            instance = this;
            DontDestroyOnLoad(gameObject);
        }

        void BuyProductID(string productId)
        {
            if (IsInitialized())
            {
                Product product = m_StoreController.products.WithID(productId);
                if (product != null && product.availableToPurchase)
                {
                    Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                    m_StoreController.InitiatePurchase(product);
                }
                else
                {
                    Debug.Log(
                        "BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
                }
            }
            else
            {
                Debug.Log("BuyProductID FAIL. Not initialized.");
            }
        }

        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            Debug.Log("OnInitialized: PASS");
            m_StoreController = controller;
            m_StoreExtensionProvider = extensions;
        }


        public void OnInitializeFailed(InitializationFailureReason error)
        {
            Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
        }

        public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}",
                product.definition.storeSpecificId, failureReason));
        }
    }
}