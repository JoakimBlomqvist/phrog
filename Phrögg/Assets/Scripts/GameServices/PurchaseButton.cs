﻿//Written by Joakim Blomqvist

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

namespace GameServices
{
    public class PurchaseButton : MonoBehaviour
    {
        private Dictionary<string, object> customEventParams = new Dictionary<string, object>();
        
        public enum PurchaseType
        {
            coin1000,
            ship
        };

        public PurchaseType purschaseType;

        public void ClickPurchaseButton()
        {
            switch (purschaseType)
            {
                case PurchaseType.coin1000:
                    IAPManager.instance.BuyCoins1000();
                    AnalyticsResult eventResultBuyCoins = AnalyticsEvent.Custom("Coins_Purchased", customEventParams);

                    break;
                case PurchaseType.ship:
                    IAPManager.instance.BuyShip();
                    AnalyticsResult eventResultBuyShip = AnalyticsEvent.Custom("Ship_Purchased", customEventParams);

                    break;
            }
        }



    }
}
