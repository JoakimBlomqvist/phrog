﻿//Written by: Joakim Blomqvist

using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerCoins : MonoBehaviour
{
    public int playerCoins = 0;
    public int sessionCoins = 0;
    public TMP_Text playerCoinsText;
    [SerializeField]private TMP_Text sessionCoinsText;
    private GameObject sessionCoinsObject;

    public static PlayerCoins instance;


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }
    
    private void Start()
    {
        playerCoinsText.text = "Coins: " + playerCoins;

        if (PlayerPrefs.HasKey("playerWallet"))
        {
            playerCoins = PlayerPrefs.GetInt("playerWallet");
        }
        else
        {
            PlayerPrefs.SetInt("playerWallet", playerCoins);
        }
      
        Debug.Log(PlayerPrefs.GetInt("playerWallet"));
        
    }

    //Updates current sessionCoins earned. Enemies drops coins
    private void Update()
    {
        if(sessionCoinsText != null)
        {
            sessionCoinsText.text = "Earned: " + sessionCoins;
        }
        //Checks if theres a sessioncoins text in the scene with the tag SessionCoins
        else if(GameObject.FindGameObjectWithTag("SessionCoins") != null)
        {
            sessionCoinsObject = GameObject.FindGameObjectWithTag("SessionCoins");
            sessionCoinsText = sessionCoinsObject.GetComponent<TMP_Text>();
        }

        if (playerCoinsText != null)
        {
            playerCoinsText.text = "Coins: " + playerCoins;
        }
    }
    //Double the sessionCoins earned in the play session and double it to the "player wallet"
    public void DoubleCoins()
    {
        playerCoins += sessionCoins;
        updatePlayerPrefs();
        sessionCoins = 0;

        SceneManager.LoadScene(0);
    }


    public void updatePlayerPrefs()
    {
        PlayerPrefs.SetInt("playerWallet", playerCoins);
        PlayerPrefs.Save();
    }




}
