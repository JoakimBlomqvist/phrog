﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class doubleCoin : MonoBehaviour
{
    public int coins = 100;

    public TMP_Text text;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void extraCoins()
    {
        coins *= 2;

        text.text = "Coins: " + coins;
    }
}
