﻿//Written by: August Möller

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossEntity : Entity
{
    //public bool dead = false;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "PlayerProjectile")
        {
            TakeDamage();
        }
    }
    /*public override void Die()
    {
        dead = true;
        Destroy(gameObject);
    }*/
}
