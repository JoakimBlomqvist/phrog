﻿//Written by: August Möller

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyEntity : Entity
{
    public GameObject dropitems;
    float droprate = 0.25f;
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision) //when touching the player projectile take damage
    {
        if (collision.tag == "PlayerProjectile")
        {
            TakeDamage(collision.gameObject.GetComponent<Projectile>().damage);
        }
    }

    public override void Die() //when dying randomly spawn a powerup and remove game object
    {
        if (Random.Range(0f, 1f) <= droprate)
        {
            Instantiate(dropitems, transform.position, transform.rotation);
        }
        Destroy(gameObject);
    }
}
