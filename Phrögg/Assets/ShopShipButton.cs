﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameServices;
using TMPro;
using UnityEngine;
using Image = UnityEngine.UI.Image;

public class ShopShipButton : MonoBehaviour
{
    public Character character;

    public Image Portrait;
    public TextMeshProUGUI Text;

    private bool unlocked;


    private void Awake()
    {
        Init();
    }

    private void Init()
    {
        
        Portrait.sprite = character.Portrait;
        Text.text = character.Cost.ToString();
        unlocked = GameManager.instance.IsCharacterUnlocked(character);

        if (character.Name == GameManager.instance.GetCharacter().Name)
        {
            Text.text = "ACTIVE";
        }
        else if (unlocked)
        {
            Text.text = "UNLOCKED";
        }
        
    }

    public void ButtonPressed()
    {
        if (!unlocked)
        {
            if (PlayerCoins.instance.playerCoins > character.Cost)
            {
                PlayerCoins.instance.playerCoins -= character.Cost;
                GameManager.instance.UnlockCharacter(character);
                unlocked = true;
            }
        }
        if (unlocked)
        {
            GameManager.instance.UnlockCharacter(character);
            GameManager.instance.SetCharacter(character);
            foreach (var shopButton in FindObjectsOfType<ShopShipButton>())
            {
                shopButton.Init();
            }
        }
        



    }




}
