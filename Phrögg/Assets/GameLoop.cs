﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameLoop : MonoBehaviour
{
    public GameObject player;
    public PlayerEntity playerEntity;
    public bool playerAlive = true;
    void Start()
    {
        playerEntity = player.GetComponent<PlayerEntity>();
    }

    private void Awake()
    {
        Time.timeScale = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        CheckForDeath();
    }

    public void CheckForDeath()
    {
        if (playerEntity.lives == 0 && playerAlive == true)
        {
            playerAlive = false;
            Debug.Log("Player Dead");
        }
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(1);
    }
}
