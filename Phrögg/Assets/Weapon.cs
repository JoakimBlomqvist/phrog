﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

[CreateAssetMenu(menuName = "Weapon")]
public class Weapon : ScriptableObject
{
    public List<ProjectileData> projectiles;
    public float fireRate = 0.1f;

    public virtual void FireWeapon(Vector3 pos)
    {
        foreach(var projectile in projectiles)
        {
            var newPos = new Vector2(pos.x + projectile.translate.x, pos.y + projectile.translate.y);
            
            var p = Instantiate(projectile.go, newPos, quaternion.identity); //instantiate projectile
            p.speed = projectile.speed; //set the speed of the projectile
            p.direction = projectile.direction; //set the direction
        }
    }

}

[Serializable]
public struct ProjectileData
{
    public float direction;
    public float speed;
    public Vector2 translate;
    public Projectile go;
}
